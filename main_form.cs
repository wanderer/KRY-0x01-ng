﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace KRY_0x01_ng
{
    public partial class main_form : Form
    {
        public main_form()
        {
            InitializeComponent();
        }

        char[,] arrayTable = new char[5, 5];
        char[] alphabet = "ABCDEFGHIJKLMNOPQRSTUVXYZ".ToCharArray(); // no W
        char[] nualphabet = new char[25];
        string prep_msg_str = "";



        string restore_order(string str)
        {
            string alpha_str = Regex.Replace(str, "SPACEBRO|SPACEBROX", " ");
            alpha_str = Regex.Replace(alpha_str, "COMMABRO|COMXMABRO|COMMANDBROX", ",");
            return alpha_str.Replace("DOTBRO|DOTBROX", ".");
        }

        string introduce_chaos(string str)
        {
            string nustr = Regex.Replace(str, " ", "SPACEBRO");
            nustr = MMLib.Extensions.StringExtensions.RemoveDiacritics(nustr);
            nustr = Regex.Replace(nustr, "W", "V");
            nustr = Regex.Replace(nustr, ",", "COMMABRO");
            return nustr.Replace(".", "DOTBRO");
        }

        string pad(string str)
        {
            string padded_str = "";
            if (str.Length > 0)
            {
                int i = 0;
                while (i < str.Length)
                {
                    char first_char = str[i];
                    i++;
                    char second_char;
                    if ((i == str.Length || str[i] == first_char) && (first_char != 'X'))
                    {
                        second_char = 'X';
                    }
                    else if ((i == str.Length || str[i] == first_char) && (first_char != 'Q'))
                    {
                        second_char = 'Q';
                    }
                    else if ((i == str.Length || str[i] == first_char) && (first_char != 'Z'))
                    {
                        second_char = 'Z';
                    }
                    else
                    {
                        second_char = str[i];
                        i++;
                    }
                    padded_str += first_char;
                    padded_str += second_char;
                }
                if (padded_str.Length % 2 != 0)
                {
                    padded_str = pad(padded_str);
                }
            }
            return padded_str;
        }


        bool check_key(TextBox tb_k, TextBox tb_pk, TextBox tb_na)
        {
            if (tb_k.Text.Length == 0)
            {
                MessageBox.Show("Empty key.", "Warning");
                return false;
            }

            string str_to_check = MMLib.Extensions.StringExtensions.RemoveDiacritics(tb_k.Text.ToUpper());
            str_to_check = String.Join("", str_to_check.Distinct());
            if (str_to_check.Length < 9)
            {
                /* because why not, 8 is still lame */
                MessageBox.Show($"The key is too short ({str_to_check.Length} characters).\nKey requirements: 9-25 unique alphabetic characters", "Error: short key");
                return false;
            }
            else if (str_to_check.Length > 25)
            {
                MessageBox.Show("The key is too long", "Warning");
                return false;
            }

            Match match = Regex.Match(str_to_check, @"\d|\s+|W");
            if (match.Success)
            {
                MessageBox.Show("Only alphabetic characters (with the exception of 'W') are allowed.\nCheck the key for numbers, symbols or tab whitespace and remove them before continuing.", "Error: invalid chars in key");
                return false;
            }

            TextBox tb_pruned_k = tb_pk;
            TextBox tb_nualph = tb_na;
            tb_pruned_k.Text = str_to_check;
            char[] transient_alph = alphabet.Except(str_to_check.ToArray()).ToArray();
            string transientalph_str = "";
            for (int i = 0; i < transient_alph.Length; i++)
            {
                transientalph_str += transient_alph[i];
            }
            string nualph_str = str_to_check + transientalph_str;
            tb_nualph.Text = nualph_str;
            nualphabet = nualph_str.ToCharArray();
            return true;
        }


        void fill_array_table()
        {
            int arrtb_gl = arrayTable.GetLength(0);

            int c = 0;
            for (int i = 0; i < arrtb_gl; i++)
            {
                for (int j = 0; j < arrtb_gl; j++)
                {
                    arrayTable[i, j] = Convert.ToChar(nualphabet[c]);
                    c++;
                }
            }

            dataGridView1.Rows.Clear();
            dataGridView1.Columns.Clear();

            dataGridView1.ColumnCount = arrtb_gl;
            for (int i = 0; i < 5; i++)
            {
                string[] one_row_plz = new string[arrtb_gl];
                for (int j = 0; j < arrtb_gl; j++)
                {
                    one_row_plz[j] = Convert.ToString(arrayTable[i,j]);
                }
                dataGridView1.Rows.Add(one_row_plz);
            }
            label8.Visible = true;
            return;
        }


        void check_message(TextBox tb_m)
        {
            if (tb_m.Text.Length == 0)
            {
                MessageBox.Show("Empty message, nothing to do.", "Warning");
                return;
            }
            string msg = introduce_chaos(tb_m.Text.ToUpper());
            msg = MMLib.Extensions.StringExtensions.RemoveDiacritics(msg);
            Match match = Regex.Match(msg, @"\d|\s+");
            if (match.Success)
            {
                MessageBox.Show("Only alphabetic characters, spaces, commas (,) and dots (.) are allowed.\nCheck the message for numbers, symbols or tab whitespace and remove them before continuing.\nAlso, \"W\" will be replaced by \"V\".", "Error: invalid chars in message");
                return;
            }
            textBox5.Text = msg;
            return;
        }

        bool check_cryptmessage(TextBox tb_m)
        {
            if (tb_m.Text.Length == 0)
            {
                MessageBox.Show("Empty message, nothing to do.", "Warning");
                return false;
            }
            string msg = tb_m.Text.ToUpper();
            msg = MMLib.Extensions.StringExtensions.RemoveDiacritics(msg);
            Match match = Regex.Match(msg, @"\d|\s+|W");
            if (match.Success)
            {
                MessageBox.Show("Only alphabetic characters (with the exception of W) are allowed.\nCheck the cryptmessage for numbers, symbols or tab whitespace and remove them before continuing.", "Error: invalid chars in cryptmessage");
                return false;
            }
            if (tb_m.Text.Length % 2 != 0)
            {
                MessageBox.Show("This pretty sure can't be a message I can deal with (uneven number of characters).", "Error");
                return false;
            }
            textBox9.Text = msg;
            return true;
        }

        string msg_hack_up(string msg)
        {
            string digraph_msg = "";
            for (int i = 0; i < msg.Length - 1; i+=2)
            {
                digraph_msg += msg.Substring(i, 2) + " ";
            }
            return digraph_msg.TrimEnd(" ".ToCharArray());
        }

        void prep_message(TextBox tb_m)
        {
            string msg = pad(tb_m.Text);
            prep_msg_str = msg;
            textBox6.Text = msg_hack_up(msg);
            return;
        }


        int[] gimme_char_indices(char ch)
        {
            int[] chxy = new int[2];
            int atbgl = arrayTable.GetLength(0);
            for (int i = 0; i < atbgl; i++)
            {
                for (int j = 0; j < atbgl; j++)
                {
                    if (arrayTable[i,j].Equals(ch))
                    {
                        chxy[0] = i;
                        chxy[1] = j;
                    }
                }
            }
            return chxy;
        }


        void encrypt()
        {
            if (!check_key(textBox1, textBox2, textBox3))
            {
                return;
            }

            fill_array_table();
            check_message(textBox4);
            prep_message(textBox5);

            string cryptmessage = "";
            string transient_cryptm = "";
            string msg = prep_msg_str;
            int arrtb_gl = arrayTable.GetLength(0);

            for (int i = 0; i < msg.Length; i+=2)
            {
                char ch1 = msg[i];
                char ch2 = msg[i + 1];
                char ech1;
                char ech2;
                int[] ch1xy = gimme_char_indices(ch1);
                int[] ch2xy = gimme_char_indices(ch2);

                if (ch1xy[0] == ch2xy[0])
                {
                    ech1 = arrayTable[ch1xy[0], (ch1xy[1] + 1) % arrtb_gl];
                    ech2 = arrayTable[ch2xy[0], (ch2xy[1] + 1) % arrtb_gl];
                }
                else if (ch1xy[1] == ch2xy[1])
                {
                    ech1 = arrayTable[(ch1xy[0] + 1) % arrtb_gl, ch1xy[1]];
                    ech2 = arrayTable[(ch2xy[0] + 1) % arrtb_gl, ch2xy[1]];
                }
                else
                {
                    ech1 = arrayTable[ch1xy[0], ch2xy[1]];
                    ech2 = arrayTable[ch2xy[0], ch1xy[1]];
                }

                string resulting_digraph = ech1.ToString() + ech2.ToString();
                transient_cryptm += resulting_digraph;
            }
            for (int i = 0; i < transient_cryptm.Length; i+=5)
            {
                if (i < transient_cryptm.Length - 4)
                {
                    cryptmessage += transient_cryptm.Substring(i, 5) + " ";
                    continue;
                }
                cryptmessage += transient_cryptm.Substring(i, transient_cryptm.Length-i);
            }
            textBox7.Text = cryptmessage;
            return;
        }



        void decrypt()
        {
            if(!check_key(textBox8, textBox11, textBox10))
            {
                return;
            }
            fill_array_table();
            if (!check_cryptmessage(textBox9))
            {
                return;
            }

            string message = "";
            string crm = textBox9.Text;
            int arrtb_gl = arrayTable.GetLength(0);
            for (int i = 0; i < crm.Length; i+=2)
            {
                char ch1 = crm[i];
                char ch2 = crm[i + 1];
                char ech1;
                char ech2;
                int[] ch1xy = gimme_char_indices(ch1);
                int[] ch2xy = gimme_char_indices(ch2);

                if (ch1xy[0] == ch2xy[0])
                {
                    /* c# neg modulo issue hack */
                    ech1 = arrayTable[ch1xy[0], (ch1xy[1] + 4) % arrtb_gl];
                    ech2 = arrayTable[ch2xy[0], (ch2xy[1] + 4) % arrtb_gl];
                }
                else if (ch1xy[1] == ch2xy[1])
                {
                    ech1 = arrayTable[(ch1xy[0] + 4) % arrtb_gl, ch1xy[1]];
                    ech2 = arrayTable[(ch2xy[0] + 4) % arrtb_gl, ch2xy[1]];
                }
                else
                {
                    ech1 = arrayTable[ch1xy[0], ch2xy[1]];
                    ech2 = arrayTable[ch2xy[0], ch1xy[1]];
                }

                string resulting_digraph = ech1.ToString() + ech2.ToString();
                message += resulting_digraph;
            }
            textBox13.Text = message;
            message = restore_order(message);
            textBox12.Text = message;
            return;
        }




        private void button1_Click(object sender, EventArgs e)
        {
            /* encrypt button */
            encrypt();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            /* check key button */
            bool cool = check_key(textBox1, textBox2, textBox3);
            if (!cool)
            {
                return;
            }
            fill_array_table();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            /* check decryption key*/
            bool cool = check_key(textBox8, textBox11, textBox10);
            if (!cool)
            {
                return;
            }
            fill_array_table();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            /* decrypt */
            decrypt();
        }

        private void button5_Click(object sender, EventArgs e)
        {
            /* copy for decryption (tb7 --> tb9) */
            string cryptmsg = textBox7.Text.Trim();
            cryptmsg = String.Join("", cryptmsg);
            cryptmsg = Regex.Replace(cryptmsg, @"\s+|", "");
            textBox9.Text = cryptmsg;
            MessageBox.Show("great success","cryptmsg has been copied");
        }
    }
}
